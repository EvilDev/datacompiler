﻿using System.Configuration;
using System.IO;
using System.Linq;

namespace DataCompiler
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var directory = new DirectoryInfo(args[0]);

            var files = directory.GetFiles(ConfigurationManager.AppSettings[AppSettings.FileNamePattern], SearchOption.AllDirectories);

            var parser = new ExcelParser();

            using (var stream = new FileStream(args[1], FileMode.OpenOrCreate))
            {
                using (var writer = new StreamWriter(stream))
                {
                    foreach (var file in files)
                    {
                        var data = parser.Parse(file.FullName);

                        foreach (var row in data)
                        {
                            writer.WriteLine(string.Join(",", row.ToArray()));
                        }
                    }
                }
            }
        }
    }
}