﻿using System;

namespace DataCompiler
{
    public static class ObjectExtensions
    {
        public static T As<T>(this object obj)
        {
            return (T)Convert.ChangeType(obj, typeof(T));
        }
    }
}