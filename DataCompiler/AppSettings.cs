﻿namespace DataCompiler
{
    public static class AppSettings
    {
        public const string FileNamePattern = "FileNamePattern";

        public const string EndCell = "EndCell";

        public const string StartCell = "StartCell";

        public const string TitleCell = "TitleCell";
    }
}