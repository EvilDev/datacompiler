﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace DataCompiler.App
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var directory = new DirectoryInfo(BaseDirectoryTextBox.Text);

            var files = directory.GetFiles(ConfigurationManager.AppSettings[AppSettings.FileNamePattern], SearchOption.AllDirectories);

            var parser = new ExcelParser();

            using (var stream = new FileStream(OutputFileTextBox.Text, FileMode.OpenOrCreate))
            {
                using (var writer = new StreamWriter(stream))
                {
                    foreach (var file in files)
                    {
                        var data = parser.Parse(file.FullName);

                        foreach (var row in data)
                        {
                            writer.WriteLine(string.Join(",", row.ToArray()));
                        }
                    }
                }
            }
        }
    }
}
