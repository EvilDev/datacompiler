﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using Microsoft.Office.Interop.Excel;

namespace DataCompiler
{
    public class ExcelParser
    {
        private readonly string _titleCell;

        private string _startCell;

        private string _endCell;

        public ExcelParser()
        {
            _titleCell = ConfigurationManager.AppSettings[AppSettings.TitleCell];
            _startCell = ConfigurationManager.AppSettings[AppSettings.StartCell];
            _endCell = ConfigurationManager.AppSettings[AppSettings.EndCell];
        }

        public IEnumerable<IEnumerable<string>> Parse(string filePath)
        {
            var rowCollection = new List<IEnumerable<string>>();
            Application app = null;

            try
            {
                app = new Application();
                var workbook = app.Workbooks.Open(filePath);
                var sheet = workbook.Sheets.Cast<Worksheet>().First();
                var range = sheet.Range[this._startCell, this._endCell];

                var title = sheet.Range[_titleCell, _titleCell].Value2.ToString();

                foreach (Range row in range.Rows)
                {
                    var list = new List<string> { title };

                    list.AddRange(row.Cells.Cast<Range>().Select(cell => cell.Value2.ToString()));

                    rowCollection.Add(list);
                }

                return rowCollection;
            }
            finally
            {
                if (app != null)
                {
                    app.Quit();
                }
            }
        }
    }
}